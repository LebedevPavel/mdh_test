const express = require('express')
const socketio = require('socket.io')
const http = require('http')
const path = require('path')

const app = express()
const server = http.createServer(app)
const io = socketio(server)

const port = process.env.PORT || 3000
const staticPath = path.join(__dirname, '../client')

app.use(express.static(staticPath))

io.on('connection', (socket) => {
    console.log(`New client connected with id ${socket.id}`)

    const timerId = setInterval(() => {
        const date = new Date()
        const dateString = date.toLocaleTimeString()
        socket.emit('newTableData', {
            id: socket.id,
            time: dateString
        })
    }, 1000);

    socket.on('disconnect', () => {
        clearInterval(timerId)
        console.log(`Client id: ${socket.id} was disconnected.`)
    })
})

server.listen(port, () => {
    console.log(`Server is listening on port ${port}`)
})