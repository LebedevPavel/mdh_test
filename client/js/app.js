const socket = io()

socket.on('newTableData', (data) => {
    const mainTable = document.querySelector('#mainTable')
    
    const row = mainTable.insertRow()
    const idCell = row.insertCell(0)
    const dateCell = row.insertCell(1)

    idCell.innerHTML = data.id
    dateCell.innerHTML = data.time
})